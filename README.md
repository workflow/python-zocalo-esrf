# ESRF Zocalo Implmentation

Services and wrappers for ESRF usage of zocalo

# Services

## BlissFileWatcher
Watches a bliss h5 file and notifies on points change. Can also watch lima images in either h5 or individual file format

## EXAFS_QA
EXAFS scan quality indicator. Monitors an EXAFS signal and calculates related noise

# Wrappers

## PyMCAFitter
Fits mca spectra based on sample content
