from setuptools import setup
import os

version = os.environ.get("MODULEVER", "0.0")

setup(
    name="zocalo-esrf",
    version=version,
    author="ESRF BCU",
    author_email="bcu@esrf.fr",
    packages=["zocalo_esrf"],
    tests_require=["pytest>=3.1", "pytest-mock"],
    install_requires=[
        "workflows>=1.7",
        "zocalo",
        "procrunner",
        # exafs-qa
        "h5py",
        "pypushflow==0.2.0b2",
        "est==0.3.0",
        "xraylarch",
        # pymca-fitter
        "pint",
        "chemparse",
        "pymca",
        "spectrocrunch",
    ],
    entry_points={
        "workflows.services": [
            "EXAFS_QA = zocalo_esrf.services.exafs_qa.main:EXAFS_QA",
            "EXAFS_QALocal = zocalo_esrf.services.exafs_qa.local:EXAFS_QALocal",
            "BlissFilewatcher = zocalo_esrf.services.blissfilewatcher:BlissFileWatcher",
        ],
        "zocalo.wrappers": ["pymca = zocalo_esrf.wrappers.pymca.wrapper:PyMCAFitter"],
        "zocalo.system_tests": [
            "blissfilewatcher = zocalo_esrf.system_tests.blissfilewatcher:BlissFilewatcherService",
            "exafs_qa = zocalo_esrf.system_tests.exafs_qa:EXAFSQAService",
        ],
    },
    zip_safe=False,
)
