import collections
import pytest
import os
import time
from unittest import mock

import workflows.transport.common_transport
from workflows.recipe.wrapper import RecipeWrapper

from zocalo_esrf.services.blissfilewatcher import BlissFileWatcher, is_file_selected


def generate_recipe_message(parameters, output):
    """Helper function for tests."""
    message = {
        "recipe": {
            1: {
                "service": "Bliss Filewatcher",
                "queue": "filewatcher",
                "parameters": parameters,
                "output": output,
            },
            2: {"service": "output", "queue": "transient.output"},
            "start": [(1, [])],
        },
        "recipe-pointer": 1,
        "recipe-path": [],
        "environment": {
            "ID": mock.sentinel.GUID,
            "source": mock.sentinel.source,
            "timestamp": mock.sentinel.timestamp,
        },
        "payload": mock.sentinel.payload,
    }
    return message


def test_filewatcher_watch(mocker):
    mock_transport = mock.Mock()
    filewatcher = BlissFileWatcher()
    setattr(filewatcher, "_transport", mock_transport)
    filewatcher.initializing()

    points = 26
    select = 5
    every = 5

    hdf5_file = os.path.dirname(__file__) + "/data/bliss/scan.h5"
    hdf5_path = "1.1/measurement"

    t = mock.create_autospec(workflows.transport.common_transport.CommonTransport)
    m = generate_recipe_message(
        parameters={
            "hdf5": hdf5_file,
            "hdf5-path": hdf5_path,
            "expected-points": points,
            "timeout": 0.5,
            "log-timeout-as-info": True,
            "burst-wait": 5,
        },
        output={"any": 1, f"select-{select}": 1, f"every-{every}": 1},
    )
    rw = RecipeWrapper(message=m, transport=t)
    send_to = mocker.spy(rw, "send_to")
    filewatcher.watch_files(rw, {"some": "header"}, mock.sentinel.message)

    # print(send_to.mock_calls)
    for i in range(points):
        if i == 0:
            send_to.assert_any_call(
                "first",
                {"hdf5": hdf5_file, "hdf5-path": hdf5_path, "hdf5-index": i},
                transaction=mock.ANY,
            )

        if is_file_selected(i + 1, select, points):
            send_to.assert_any_call(
                f"select-{select}",
                {"hdf5": hdf5_file, "hdf5-path": hdf5_path, "hdf5-index": i},
                transaction=mock.ANY,
            )

        if i % every == 0:
            send_to.assert_any_call(
                f"every-{every}",
                {"hdf5": hdf5_file, "hdf5-path": hdf5_path, "hdf5-index": i},
                transaction=mock.ANY,
            )

        send_to.assert_any_call(
            "every",
            {"hdf5": hdf5_file, "hdf5-path": hdf5_path, "hdf5-index": i},
            transaction=mock.ANY,
        )

    send_to.assert_any_call(
        "finally",
        {"points-expected": points, "points-seen": points, "success": True},
        transaction=mock.ANY,
    )


def test_filewatcher_watch_timeout(mocker):
    mock_transport = mock.Mock()
    filewatcher = BlissFileWatcher()
    setattr(filewatcher, "_transport", mock_transport)
    filewatcher.initializing()

    hdf5_file = os.path.dirname(__file__) + "/data/bliss/scan.h5"
    hdf5_path = "1.1/measurement"
    points = 27
    timeout = 0.05

    t = mock.create_autospec(workflows.transport.common_transport.CommonTransport)
    m = generate_recipe_message(
        parameters={
            "hdf5": hdf5_file,
            "hdf5-path": hdf5_path,
            "expected-points": points,
            "timeout": timeout,
            "log-timeout-as-info": True,
        },
        output={"any": 1, "timeout": 1},
    )
    rw = RecipeWrapper(message=m, transport=t)
    send_to = mocker.spy(rw, "send_to")
    filewatcher.watch_files(rw, {"some": "header"}, mock.sentinel.message)

    for i in range(points - 1):
        send_to.assert_any_call(
            "every",
            {"hdf5": hdf5_file, "hdf5-path": hdf5_path, "hdf5-index": i},
            transaction=mock.ANY,
        )

    # Sleep in order that we can hit the timeout
    time.sleep((points) * timeout)

    filewatcher.watch_files(
        rw, {"some": "header"}, t.send.mock_calls[-1][1][1]["payload"]
    )

    send_to.assert_has_calls(
        [
            mock.call(
                "timeout",
                {
                    "hdf5": hdf5_file,
                    "hdf5-path": hdf5_path,
                    "hdf5-index": 26,
                    "success": False,
                },
                transaction=mock.ANY,
            ),
            mock.call(
                "any",
                {"points-expected": points, "points-seen": 26},
                transaction=mock.ANY,
            ),
            mock.call(
                "finally",
                {"points-expected": points, "points-seen": 26, "success": False},
                transaction=mock.ANY,
            ),
        ]
    )


def test_filewatcher_watch_sequence(mocker):
    mock_transport = mock.Mock()
    filewatcher = BlissFileWatcher()
    setattr(filewatcher, "_transport", mock_transport)
    filewatcher.initializing()

    points = 26

    hdf5_file = os.path.dirname(__file__) + "/data/bliss/scan_seq.h5"
    hdf5_path = "1.1/measurement"
    new_path = "4.1/measurement"

    t = mock.create_autospec(workflows.transport.common_transport.CommonTransport)
    m = generate_recipe_message(
        parameters={
            "hdf5": hdf5_file,
            "hdf5-path": hdf5_path,
            "expected-points": points,
            "timeout": 0.5,
            "log-timeout-as-info": True,
            "burst-wait": 5,
        },
        output={"any": 1},
    )
    rw = RecipeWrapper(message=m, transport=t)
    send_to = mocker.spy(rw, "send_to")
    filewatcher.watch_files(rw, {"some": "header"}, mock.sentinel.message)

    for i in range(points):
        if i == 0:
            send_to.assert_any_call(
                "first",
                {"hdf5": hdf5_file, "hdf5-index": i, "hdf5-path": new_path},
                transaction=mock.ANY,
            )

        send_to.assert_any_call(
            "every",
            {"hdf5": hdf5_file, "hdf5-index": i, "hdf5-path": new_path},
            transaction=mock.ANY,
        )

    send_to.assert_any_call(
        "finally",
        {"points-expected": points, "points-seen": points, "success": True},
        transaction=mock.ANY,
    )


def test_filewatcher_watch_lima(mocker):
    mock_transport = mock.Mock()
    filewatcher = BlissFileWatcher()
    setattr(filewatcher, "_transport", mock_transport)
    filewatcher.initializing()

    points = 6

    hdf5_file = os.path.dirname(__file__) + "/data/lima/scan_lima.h5"
    hdf5_path = "1.1/measurement"
    lima_path = os.path.dirname(__file__) + "/data/lima/scan0001/lima_simulator_"

    t = mock.create_autospec(workflows.transport.common_transport.CommonTransport)
    m = generate_recipe_message(
        parameters={
            "hdf5": hdf5_file,
            "hdf5-path": hdf5_path,
            "image-path": lima_path,
            "expected-points": points,
            "timeout": 0.5,
            "log-timeout-as-info": True,
            "burst-wait": 5,
        },
        output={"any": 1},
    )
    rw = RecipeWrapper(message=m, transport=t)
    send_to = mocker.spy(rw, "send_to")
    filewatcher.watch_files(rw, {"some": "header"}, mock.sentinel.message)

    for i in range(points):
        image_file = f"{lima_path}000{(i // 5)}.h5"
        if i == 0:
            send_to.assert_any_call(
                "first",
                {
                    "hdf5": hdf5_file,
                    "hdf5-path": hdf5_path,
                    "hdf5-index": i,
                    "image-file": image_file,
                },
                transaction=mock.ANY,
            )

        send_to.assert_any_call(
            "every",
            {
                "hdf5": hdf5_file,
                "hdf5-path": hdf5_path,
                "hdf5-index": i,
                "image-file": image_file,
            },
            transaction=mock.ANY,
        )

    send_to.assert_any_call(
        "finally",
        {"points-expected": points, "points-seen": points, "success": True},
        transaction=mock.ANY,
    )


def test_filewatcher_watch_lima_individual(mocker, tmp_path):
    mock_transport = mock.Mock()
    filewatcher = BlissFileWatcher()
    setattr(filewatcher, "_transport", mock_transport)
    filewatcher.initializing()

    points = 6
    files = [tmp_path / f"lima_000{i}.edf" for i in range(points)]

    hdf5_file = os.path.dirname(__file__) + "/data/lima/scan_lima.h5"
    hdf5_path = "1.1/measurement"
    lima_path = f"{tmp_path}/lima_"

    t = mock.create_autospec(workflows.transport.common_transport.CommonTransport)
    m = generate_recipe_message(
        parameters={
            "hdf5": hdf5_file,
            "hdf5-path": hdf5_path,
            "image-path": lima_path,
            "expected-points": points,
            "timeout": 0.5,
            "log-timeout-as-info": True,
            "burst-wait": 5,
        },
        output={"any": 1},
    )
    rw = RecipeWrapper(message=m, transport=t)
    send_to = mocker.spy(rw, "send_to")
    filewatcher.watch_files(rw, {"some": "header"}, mock.sentinel.message)

    for i, f in enumerate(files):
        f.write_text("content")
        filewatcher.watch_files(
            rw, {"some": "header"}, t.send.mock_calls[-1][1][1]["payload"]
        )
        if i == 0:
            send_to.assert_any_call(
                "first",
                {
                    "hdf5": hdf5_file,
                    "hdf5-path": hdf5_path,
                    "hdf5-index": i,
                    "image-file": str(f),
                },
                transaction=mock.ANY,
            )

        send_to.assert_any_call(
            "every",
            {
                "hdf5": hdf5_file,
                "hdf5-path": hdf5_path,
                "hdf5-index": i,
                "image-file": str(f),
            },
            transaction=mock.ANY,
        )

    send_to.assert_any_call(
        "finally",
        {"points-expected": points, "points-seen": points, "success": True},
        transaction=mock.ANY,
    )


def test_filewatcher_watch_list(mocker, tmp_path):
    mock_transport = mock.Mock()
    filewatcher = BlissFileWatcher()
    setattr(filewatcher, "_transport", mock_transport)
    filewatcher.initializing()
    t = mock.create_autospec(workflows.transport.common_transport.CommonTransport)
    files = [tmp_path / "header", tmp_path / "end"]
    m = generate_recipe_message(
        parameters={
            "list": [str(f) for f in files],
            "timeout": 0.5,
            "log-timeout-as-info": True,
            "burst-wait": 5,
        },
        output={"any": 1},
    )
    rw = RecipeWrapper(message=m, transport=t)
    # Spy on the rw.send_to method
    send_to = mocker.spy(rw, "send_to")
    checkpoint = mocker.spy(rw, "checkpoint")
    filewatcher.watch_files(rw, {"some": "header"}, mock.sentinel.message)
    checkpoint.assert_any_call(
        {"filewatcher-status": {"seen-files": 0, "start-time": mock.ANY}},
        delay=5,
        transaction=mock.ANY,
    )
    for i, f in enumerate(files):
        f.write_text("content")
        filewatcher.watch_files(
            rw, {"some": "header"}, t.send.mock_calls[-1][1][1]["payload"]
        )
        if i == 0:
            send_to.assert_any_call(
                "first",
                {"file": str(f), "file-list-index": i + 1},
                transaction=mock.ANY,
            )
        send_to.assert_has_calls(
            [
                mock.call(
                    "every",
                    {"file": str(f), "file-list-index": i + 1},
                    transaction=mock.ANY,
                ),
                mock.call(
                    i + 1,
                    {"file": str(f), "file-list-index": i + 1},
                    transaction=mock.ANY,
                ),
                mock.call(
                    f"{i + 1}",
                    {"file": str(f), "file-list-index": i + 1},
                    transaction=mock.ANY,
                ),
            ],
            any_order=True,
        )
    send_to.assert_has_calls(
        [
            mock.call(
                "last",
                {"file": str(files[-1]), "file-list-index": len(files)},
                transaction=mock.ANY,
            ),
            mock.call(
                "any",
                {"files-expected": len(files), "files-seen": len(files)},
                transaction=mock.ANY,
            ),
            mock.call(
                "finally",
                {
                    "files-expected": len(files),
                    "files-seen": len(files),
                    "success": True,
                },
                transaction=mock.ANY,
            ),
        ],
        any_order=True,
    )


def test_filewatcher_watch_list_timeout(mocker, tmp_path):
    mock_transport = mock.Mock()
    filewatcher = BlissFileWatcher()
    setattr(filewatcher, "_transport", mock_transport)
    filewatcher.initializing()
    t = mock.create_autospec(workflows.transport.common_transport.CommonTransport)
    files = [tmp_path / "header", tmp_path / "end"]
    m = generate_recipe_message(
        parameters={
            "list": [str(f) for f in files],
            "timeout": 0.5,
            "log-timeout-as-info": True,
        },
        output={"any": 1},
    )
    rw = RecipeWrapper(message=m, transport=t)
    send_to = mocker.spy(rw, "send_to")
    filewatcher.watch_files(rw, {"some": "header"}, mock.sentinel.message)
    files[0].write_text("content")
    filewatcher.watch_files(
        rw, {"some": "header"}, t.send.mock_calls[-1][1][1]["payload"]
    )
    send_to.assert_has_calls(
        [
            mock.call(
                "first",
                {"file": str(files[0]), "file-list-index": 1},
                transaction=mock.ANY,
            ),
            mock.call(
                "every",
                {"file": str(files[0]), "file-list-index": 1},
                transaction=mock.ANY,
            ),
            mock.call(
                1, {"file": str(files[0]), "file-list-index": 1}, transaction=mock.ANY
            ),
            mock.call(
                "1", {"file": str(files[0]), "file-list-index": 1}, transaction=mock.ANY
            ),
        ]
    )
    # Sleep in order that we can hit the timeout
    time.sleep(2)
    filewatcher.watch_files(
        rw, {"some": "header"}, t.send.mock_calls[-1][1][1]["payload"]
    )
    send_to.assert_has_calls(
        [
            mock.call(
                "timeout",
                {"file": str(files[1]), "file-list-index": 2, "success": False},
                transaction=mock.ANY,
            ),
            mock.call(
                "any", {"files-expected": 2, "files-seen": 1}, transaction=mock.ANY
            ),
            mock.call(
                "finally",
                {"files-expected": 2, "files-seen": 1, "success": False},
                transaction=mock.ANY,
            ),
        ]
    )


def test_parse_everys():
    assert BlissFileWatcher._parse_everys({"every": 2}) == {}
    assert BlissFileWatcher._parse_everys({"every-1": 3}) == {1: "every-1"}
    assert BlissFileWatcher._parse_everys({"every-2": 4}) == {2: "every-2"}


def test_parse_selections():
    assert BlissFileWatcher._parse_selections({"select": 2}) == {}
    assert BlissFileWatcher._parse_selections({"select-1": 3}) == {1: "select-1"}
    assert BlissFileWatcher._parse_selections({"select-2": 4}) == {2: "select-2"}


@pytest.mark.parametrize(
    "nth_file,expected",
    [
        (1, ["first", "every", 1, "1", "select-2", "every-3"]),
        (2, ["every", 2, "2"]),
        (4, ["every", 4, "4", "every-3"]),
        (9, ["every", 9, "9", "select-2", "last"]),
    ],
)
def test_notify_for_found_file(nth_file, expected):
    notify_function = mock.Mock()
    BlissFileWatcher._notify_for_found_file(
        nth_file=nth_file,
        filecount=9,
        selections={2: "select-2"},
        everys={3: "every-3"},
        notify_function=notify_function,
    )
    for notify in expected:
        notify_function.assert_any_call(notify)
    for notify in {"first", "every", nth_file, "select-2", "every-3", "last"} - set(
        expected
    ):
        with pytest.raises(AssertionError):
            notify_function.assert_any_call(notify)


@pytest.mark.parametrize("select_n_images", (151, 250))
def test_file_selection(select_n_images):
    select_n_images = 250
    for filecount in list(range(1, 255)) + list(range(3600, 3700)):

        def selection(x):
            return is_file_selected(x, select_n_images, filecount)

        ls = list(filter(selection, range(1, filecount + 1)))

        # Check that correct number of images were selected
        assert len(ls) == min(filecount, select_n_images)

        # Check that selection was evenly distributed
        if filecount > 1:
            diffs = [n - ls[i - 1] for i, n in enumerate(ls) if i]
            assert 1 <= len(collections.Counter(diffs)) <= 2, (filecount, diffs)
