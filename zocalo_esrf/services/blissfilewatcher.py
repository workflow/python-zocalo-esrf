import contextlib
import os
import re
import time
import math
import glob

import workflows.recipe
from workflows.services.common_service import CommonService

import silx.io.h5py_utils
from silx.utils.retry import RetryTimeoutError


def is_file_selected(file_number, selection, total_files):
    """
    Checks if item number 'file_number' is in a list of 'selection'
    evenly spread out items out of a list of 'total_files' items,
    without constructing the full list of selected items.

    :param: file_number: positive number between 1 and total_files
    :param: selection: number of files to be selected out of total_files
    :param: total_files: number of total files
    :return: True if file_number would be selected, False otherwise.
    """
    return total_files <= selection or file_number in (
        total_files,
        1
        + round(file_number * (selection - 1) // total_files)
        * total_files
        // (selection - 1),
    )


class _Profiler:
    """
    A helper class that can record summary statistics on time spent in
    code blocks. Example usage:

    profiler = _Profiler()
    with profiler.record():
        ...
    print(profiler.mean)
    print(profiler.max)
    """

    def __init__(self):
        self._timing_max = None
        self._timing_sum = None
        self._timing_count = None

    @contextlib.contextmanager
    def record(self):
        start = time.time()
        try:
            yield
        finally:
            runtime = time.time() - start
            if self._timing_count:
                self._timing_count += 1
                self._timing_sum += runtime
                if runtime > self._timing_max:
                    self._timing_max = runtime
            else:
                self._timing_count = 1
                self._timing_sum = runtime
                self._timing_max = runtime

    @property
    def max(self):
        return self._timing_max

    @property
    def mean(self):
        if self._timing_count:
            return self._timing_sum / self._timing_count
        else:
            return None


@silx.io.h5py_utils.retry(retry_timeout=2)
def get_hdf5_current_point(file, path):
    """Iterate through hdf5
    
    Find smallest number of points in all datasets

    Args:
        file(str): Hdf5 file path
        path(str): Path to data within hdf5 file

    Returns
        min_points(int): Minimum number of points available
    """
    with silx.io.h5py_utils.File(file) as h5:
        group = h5[path]
        min_points = math.inf
        names = list(group.keys())
        for dataset in names:
            try:
                d = group[dataset]
            except KeyError:
                continue  # link destination does not exist yet

            point = d.shape[0]
            if point < min_points:
                min_points = point

        return min_points


@silx.io.h5py_utils.retry(retry_timeout=2)
def get_last_scan(file, path, log):
    """Get the latest scan from a hdf5 file if the first scan is a sequence"""
    with silx.io.h5py_utils.File(file) as h5:
        base = h5["1.1"]
        root = h5["1.1/measurement"]

        scan_numbers = None
        if "scan_numbers" in root:
            scan_numbers = root["scan_numbers"][()]

        if base["title"][()] == b"sequence_of_scans" and scan_numbers is not None:
            if len(scan_numbers) > 0:
                new_path = f"{scan_numbers[-1]}.1/measurement"
                log.info(
                    f"File is sequence of scans, changing path to latest scan: {new_path}"
                )
                return new_path
            else:
                log.info("Scan numbers has zero length for %s", file)

        return path


@silx.io.h5py_utils.retry(retry_timeout=2)
def get_lima_current_point(image_path, os_stat_profiler):
    """Get the latest point from a lima device"""
    total = 0
    image_containers = sorted(glob.glob(image_path + "*"))

    image_files = {}
    for container in image_containers:
        _, ext = os.path.splitext(container)

        # If the file is a h5 search for data within
        if ext == ".h5":
            with silx.io.h5py_utils.File(container) as h5:
                data = h5["entry_0000/measurement/data"]
                found = data.shape[0]
                for i in range(found):
                    image_files[total + i] = container
                total += found

        # For individual images check they exist
        else:
            result = re.match(image_path + r"(\d+).", container)
            with os_stat_profiler.record():
                if os.path.isfile(container):
                    image_files[int(result[1])] = container
                    total += 1

    return total, image_files


class BlissFileWatcher(CommonService):
    """
    A service that waits for files to arrive on disk and notifies interested
    parties when they do, or don't.
    """

    # Human readable service name
    _service_name = "Bliss Filewatcher"

    # Logger name
    _logger_name = "zocalo_esrf.services.blissfilewatcher"

    def initializing(self):
        """
        Subscribe to the filewatcher queue. Received messages must be
        acknowledged.
        """
        self.log.info("Filewatcher starting")
        workflows.recipe.wrap_subscribe(
            self._transport,
            "filewatcher",
            self.watch_files,
            acknowledgement=True,
            log_extender=self.extend_log,
        )

    def watch_files(self, rw, header, message):
        """Check for presence of files."""
        if rw.recipe_step["parameters"].get("hdf5") is not None:
            if rw.recipe_step["parameters"].get("hdf5-path") is None:
                self.log.error("Rejecting message with undefined hdf5-path")
                rw.transport.nack(header)
                return

            if rw.recipe_step["parameters"].get("expected-points") is None:
                self.log.error("Rejecting message with undefined expected-points")
                rw.transport.nack(header)
                return

            self.watch_files_hdf5(rw, header, message)

        elif rw.recipe_step["parameters"].get("list") is not None:
            self.watch_files_list(rw, header, message)

        else:
            self.log.error("Rejecting message with unknown watch target")
            rw.transport.nack(header)

    @staticmethod
    def _parse_everys(outputs):
        """
        Returns a dictionary of integers to destination names for requested
        file periods.

        :param: outputs: An iterable of strings. Those starting with "every-"
                         are picked out and parsed as "every-$number".
        :return: A dictionary of {$number: "every-$number"} entries.
        """
        # Identify periods to notify for
        everys = [k for k in outputs if isinstance(k, str) and k.startswith("every-")]
        return {int(k[6:]): k for k in everys}

    @staticmethod
    def _parse_selections(outputs):
        """
        Returns a dictionary of integers to destination names for requested
        file selections.

        :param: outputs: An iterable of strings. Those starting with "select-"
                         are picked out and parsed as "select-$number".
        :return: A dictionary of {$number: "select-$number"} entries.
        """
        # Identify selections to notify for
        selections = [
            k for k in outputs if isinstance(k, str) and k.startswith("select-")
        ]
        return {int(k[7:]): k for k in selections}

    @staticmethod
    def _notify_for_found_file(
        nth_file, filecount, selections, everys, notify_function
    ):
        """
        Sends notifications to relevant output streams.

        :param: nth_file: Number of the seen file. 1 if this is the first seen file.
        :param: filecount: Total number of files.
        :param: selections: Dictionary of {int: str} entries pointing to output
                            streams that should receive $int evenly spaced files
                            out of all files.
        :param: everys: Dictionary of {int: str} entries pointing to output
                        streams that should receive every $int file, starting
                        with the first file.
        :param: notify_function: Function called for each triggered output.
        """

        # Notify for first file
        if nth_file == 1:
            notify_function("first")

        # Notify for every file
        notify_function("every")

        # Notify for last file
        if nth_file == filecount:
            notify_function("last")

        # Notify for nth file
        notify_function(nth_file)
        notify_function(str(nth_file))

        # Notify for selections
        for m, dest in selections.items():
            if is_file_selected(nth_file, m, filecount):
                notify_function(dest)

        # Notify for every-n
        for m, dest in everys.items():
            if (nth_file - 1) % m == 0:
                notify_function(dest)

    def watch_files_hdf5(self, rw, header, message):
        """Watch for hdf5 files"""
        # Check if message body contains partial results from a previous run
        status = {"seen-points": 0, "start-time": time.time()}
        if isinstance(message, dict):
            status.update(message.get("filewatcher-status", {}))

        # Keep a record of os.stat timings
        os_stat_profiler = _Profiler()

        hdf5 = rw.recipe_step["parameters"]["hdf5"]
        hdf5_path = status.get("hdf5-path", rw.recipe_step["parameters"]["hdf5-path"])
        image_path = rw.recipe_step["parameters"].get("image-path")

        point_count = int(rw.recipe_step["parameters"].get("expected-points"))

        # Identify everys ('every-N' targets) to notify for
        everys = self._parse_everys(rw.recipe_step["output"])

        # Identify selections ('select-N' targets) to notify for
        selections = self._parse_selections(rw.recipe_step["output"])

        # Conditionally acknowledge receipt of the message
        txn = rw.transport.transaction_begin()
        rw.transport.ack(header, transaction=txn)

        # Look for points
        file_exists = True
        with os_stat_profiler.record():
            if not os.path.isfile(hdf5):
                self.log.info("File does not yet exist %s", hdf5)
                points_found = 0
                file_exists = False

        # In case of sequence of scans, find latest scan,
        # only on the first message, and persist into status
        if not status.get("file_exists") and file_exists:
            try:
                hdf5_path = get_last_scan(hdf5, hdf5_path, self.log)
                status["hdf5-path"] = hdf5_path
            except KeyError as e:
                self.log.info(
                    "Unable to lookup potential sequence, cant find root: %s", e
                )
                points_found = 0
                file_exists = False
            except Exception:
                self.log.exception("Something went wrong reading the root file")
                points_found = 0
                file_exists = False

        status["file_exists"] = file_exists

        if file_exists:
            if image_path:
                try:
                    points_found, image_files = get_lima_current_point(
                        image_path, os_stat_profiler
                    )
                    points_found -= status["seen-points"]
                except RetryTimeoutError:
                    self.log.exception("Could not read lima images within timeout")
                    points_found = 0

            else:
                try:
                    points_found = (
                        get_hdf5_current_point(hdf5, hdf5_path) - status["seen-points"]
                    )
                except RetryTimeoutError:
                    self.log.exception("Could not read hdf5 file within timeout")
                    points_found = 0

            if points_found < 0:
                self.log.info(
                    "Something went wrong watching (%s@)%s%s, seen-points is greater than current points: %s",
                    hdf5_path,
                    hdf5,
                    f" [{image_path}]" if image_path else "",
                    point_count,
                )
                rw.transport.transaction_commit(txn)
                return

            for _ in range(points_found):

                def notify_function(output):
                    message = {
                        "hdf5": hdf5,
                        "hdf5-path": hdf5_path,
                        "hdf5-index": status["seen-points"],
                    }

                    if image_path:
                        try:
                            message["image-file"] = image_files[status["seen-points"]]
                        except KeyError:
                            self.log.warning(
                                f"Couldnt retrieve image-file for index {status['seen-points']}, available points {image_files.keys()}"
                            )

                    rw.send_to(
                        output, message, transaction=txn,
                    )

                self._notify_for_found_file(
                    status["seen-points"] + 1,
                    point_count,
                    selections,
                    everys,
                    notify_function,
                )
                status["seen-points"] += 1

        # Are we done?
        if status["seen-points"] >= point_count:
            # Happy days
            if status["seen-points"] > point_count:
                self.log.warning(
                    f"Found more points ({status['seen-points']}) than expected ({point_count}) for {hdf5}"
                )
            else:
                self.log.debug(f"All {point_count} points found for {hdf5}")

            extra_log = {
                "delay": time.time() - status["start-time"],
                "stat-time-max": os_stat_profiler.max,
                "stat-time-mean": os_stat_profiler.mean,
            }
            if rw.recipe_step["parameters"].get("expected-per-point-delay"):
                # Estimate unexpected delay
                try:
                    expected_delay = (
                        float(rw.recipe_step["parameters"]["expected-per-point-delay"])
                        * point_count
                    )
                except ValueError:
                    # in case the field contains "None" or equivalent un-floatable nonsense
                    self.log.warning(
                        "Ignored invalid expected-per-point-delay value (%r)",
                        rw.recipe_step["parameters"]["expected-per-point-delay"],
                    )
                else:
                    extra_log["unexpected_delay"] = max(
                        0, extra_log["delay"] - expected_delay
                    )

            self.log.info(
                "All %d points found for (%s@)%s%s after %.2f seconds.",
                point_count,
                hdf5_path,
                hdf5,
                f" [{image_path}]" if image_path else "",
                time.time() - status["start-time"],
                extra=extra_log,
            )

            rw.send_to(
                "any",
                {"points-expected": point_count, "points-seen": status["seen-points"]},
                transaction=txn,
            )
            rw.send_to(
                "finally",
                {
                    "points-expected": point_count,
                    "points-seen": status["seen-points"],
                    "success": True,
                },
                transaction=txn,
            )

            rw.transport.transaction_commit(txn)
            return

        message_delay = rw.recipe_step["parameters"].get("burst-wait")
        if points_found == 0:
            # If no images were found, check timeout conditions.
            if status["seen-points"] == 0:
                # For first file: relevant timeout is 'timeout-first', with fallback 'timeout', with fallback 1 hour
                timeout = rw.recipe_step["parameters"].get(
                    "timeout-first", rw.recipe_step["parameters"].get("timeout", 3600)
                )
                timed_out = (status["start-time"] + timeout) < time.time()
            else:
                # For subsequent images: relevant timeout is 'timeout', with fallback 1 hour
                timeout = rw.recipe_step["parameters"].get("timeout", 3600)
                timed_out = (status["last-seen"] + timeout) < time.time()

            if timed_out:
                # File watch operation has timed out.

                # Report all timeouts as warnings unless the recipe specifies otherwise
                timeoutlog = self.log.warning
                if rw.recipe_step["parameters"].get("log-timeout-as-info"):
                    timeoutlog = self.log.info

                timeoutlog(
                    "Filewatcher for (:%s@)%s%s timed out after %.2f seconds (%d points found, nothing seen for %.2f seconds)",
                    hdf5_path,
                    hdf5,
                    f" [{image_path}]" if image_path else "",
                    time.time() - status["start-time"],
                    status["seen-points"],
                    time.time() - status.get("last-seen", status["start-time"]),
                    extra={
                        "stat-time-max": os_stat_profiler.max,
                        "stat-time-mean": os_stat_profiler.mean,
                    },
                )

                # Notify for timeout
                rw.send_to(
                    "timeout",
                    {
                        "hdf5": hdf5,
                        "hdf5-path": hdf5_path,
                        "hdf5-index": status["seen-points"],
                        "success": False,
                    },
                    transaction=txn,
                )
                # Notify for 'any' target if any file was seen
                if status["seen-points"]:
                    rw.send_to(
                        "any",
                        {
                            "points-expected": point_count,
                            "points-seen": status["seen-points"],
                        },
                        transaction=txn,
                    )

                # Notify for 'finally' outcome
                rw.send_to(
                    "finally",
                    {
                        "points-expected": point_count,
                        "points-seen": status["seen-points"],
                        "success": False,
                    },
                    transaction=txn,
                )
                # Stop processing message
                rw.transport.transaction_commit(txn)
                return

            # If no timeouts are triggered, set a minimum waiting time.
            if message_delay:
                message_delay = max(1, message_delay)
            else:
                message_delay = 1

            self.log.debug(
                "No further points found for (%s@)%s%s after a total time of %.2f seconds\n"
                "%s of %s points seen so far",
                hdf5_path,
                hdf5,
                f" [{image_path}]" if image_path else "",
                time.time() - status["start-time"],
                status["seen-points"],
                point_count,
                extra={
                    "stat-time-max": os_stat_profiler.max,
                    "stat-time-mean": os_stat_profiler.mean,
                },
            )
        else:
            # Otherwise note last time progress was made
            status["last-seen"] = time.time()
            self.log.info(
                "%d  points found for (%s@)%s%s (total: %d out of %d) within %.2f seconds",
                points_found,
                hdf5_path,
                hdf5,
                f" [{image_path}]" if image_path else "",
                status["seen-points"],
                point_count,
                time.time() - status["start-time"],
                extra={
                    "stat-time-max": os_stat_profiler.max,
                    "stat-time-mean": os_stat_profiler.mean,
                },
            )

        # Send results to myself for next round of processing
        rw.checkpoint(
            {"filewatcher-status": status}, delay=message_delay, transaction=txn
        )
        rw.transport.transaction_commit(txn)

    def watch_files_list(self, rw, header, message):
        """Watch for a given list of files."""
        # Check if message body contains partial results from a previous run
        status = {"seen-files": 0, "start-time": time.time()}
        if isinstance(message, dict):
            status.update(message.get("filewatcher-status", {}))

        # List files to wait for
        filelist = rw.recipe_step["parameters"]["list"]
        filecount = len(filelist)

        # If the only entry in the list is 'None' then there are no files to
        # watch for. Bail out early and only notify on 'finally'.
        if filecount == 1 and filelist[0] is None:
            self.log.debug("Empty list encountered")
            txn = rw.transport.transaction_begin()
            rw.transport.ack(header, transaction=txn)
            rw.send_to(
                "finally",
                {"files-expected": 0, "files-seen": 0, "success": True},
                transaction=txn,
            )
            rw.transport.transaction_commit(txn)
            return

        # Identify everys ('every-N' targets) to notify for
        everys = self._parse_everys(rw.recipe_step["output"])

        # Identify selections ('select-N' targets) to notify for
        selections = self._parse_selections(rw.recipe_step["output"])

        # Conditionally acknowledge receipt of the message
        txn = rw.transport.transaction_begin()
        rw.transport.ack(header, transaction=txn)

        # Keep a record of os.stat timings
        os_stat_profiler = _Profiler()

        # Look for files
        files_found = 0
        while (
            status["seen-files"] < filecount
            and files_found < rw.recipe_step["parameters"].get("burst-limit", 100)
            and filelist[status["seen-files"]]
        ):
            with os_stat_profiler.record():
                if not os.path.isfile(filelist[status["seen-files"]]):
                    break

            files_found += 1
            status["seen-files"] += 1

            def notify_function(output):
                rw.send_to(
                    output,
                    {
                        "file": filelist[status["seen-files"] - 1],
                        "file-list-index": status["seen-files"],
                    },
                    transaction=txn,
                )

            self._notify_for_found_file(
                status["seen-files"], filecount, selections, everys, notify_function
            )

        # Are we done?
        if status["seen-files"] == filecount:
            # Happy days

            self.log.info(
                "All %d files in list found after %.1f seconds.",
                filecount,
                time.time() - status["start-time"],
                extra={
                    "stat-time-max": os_stat_profiler.max,
                    "stat-time-mean": os_stat_profiler.mean,
                },
            )

            rw.send_to(
                "any",
                {"files-expected": filecount, "files-seen": status["seen-files"]},
                transaction=txn,
            )
            rw.send_to(
                "finally",
                {
                    "files-expected": filecount,
                    "files-seen": status["seen-files"],
                    "success": True,
                },
                transaction=txn,
            )

            rw.transport.transaction_commit(txn)
            return

        if not filelist[status["seen-files"]]:
            # 'None' value or empty string encountered. Stop watching here.

            self.log.info(
                "Filewatcher stopped after encountering empty value at position %d after %.1f seconds",
                status["seen-files"] + 1,
                time.time() - status["start-time"],
            )

            # Notify for error
            rw.send_to(
                "error",
                {
                    "file": filelist[status["seen-files"]],
                    "file-list-index": status["seen-files"] + 1,
                    "success": False,
                },
                transaction=txn,
            )

            # Notify for 'any' target if any file was seen
            if status["seen-files"]:
                rw.send_to(
                    "any",
                    {"files-expected": filecount, "files-seen": status["seen-files"]},
                    transaction=txn,
                )

            # Notify for 'finally' outcome
            rw.send_to(
                "finally",
                {
                    "files-expected": filecount,
                    "files-seen": status["seen-files"],
                    "success": False,
                },
                transaction=txn,
            )
            # Stop processing message
            rw.transport.transaction_commit(txn)
            return

        message_delay = rw.recipe_step["parameters"].get("burst-wait")
        if files_found == 0:
            # If no files were found, check timeout conditions.
            if status["seen-files"] == 0:
                # For first file: relevant timeout is 'timeout-first', with fallback 'timeout', with fallback 1 hour
                timeout = rw.recipe_step["parameters"].get(
                    "timeout-first", rw.recipe_step["parameters"].get("timeout", 3600)
                )
                timed_out = (status["start-time"] + timeout) < time.time()
            else:
                # For subsequent files: relevant timeout is 'timeout', with fallback 1 hour
                timeout = rw.recipe_step["parameters"].get("timeout", 3600)
                timed_out = (status["last-seen"] + timeout) < time.time()
            if timed_out:
                # File watch operation has timed out.

                # Report all timeouts as warnings unless the recipe specifies otherwise
                timeoutlog = self.log.warning
                if rw.recipe_step["parameters"].get("log-timeout-as-info"):
                    timeoutlog = self.log.info

                timeoutlog(
                    "Filewatcher for file %s timed out after %.1f seconds (%d of %d files found, nothing seen for %.1f seconds)",
                    filelist[status["seen-files"]],
                    time.time() - status["start-time"],
                    status["seen-files"],
                    filecount,
                    time.time() - status.get("last-seen", status["start-time"]),
                    extra={
                        "stat-time-max": os_stat_profiler.max,
                        "stat-time-mean": os_stat_profiler.mean,
                    },
                )

                # Notify for timeout
                rw.send_to(
                    "timeout",
                    {
                        "file": filelist[status["seen-files"]],
                        "file-list-index": status["seen-files"] + 1,
                        "success": False,
                    },
                    transaction=txn,
                )
                # Notify for 'any' target if any file was seen
                if status["seen-files"]:
                    rw.send_to(
                        "any",
                        {
                            "files-expected": filecount,
                            "files-seen": status["seen-files"],
                        },
                        transaction=txn,
                    )

                # Notify for 'finally' outcome
                rw.send_to(
                    "finally",
                    {
                        "files-expected": filecount,
                        "files-seen": status["seen-files"],
                        "success": False,
                    },
                    transaction=txn,
                )
                # Stop processing message
                rw.transport.transaction_commit(txn)
                return

            # If no timeouts are triggered, set a minimum waiting time.
            if message_delay:
                message_delay = max(1, message_delay)
            else:
                message_delay = 1
            self.log.debug(
                (
                    "No further files in list found after a total time of {time:.1f} seconds\n"
                    "{files_seen} of {files_total} files seen so far"
                ).format(
                    time=time.time() - status["start-time"],
                    files_seen=status["seen-files"],
                    files_total=filecount,
                ),
                extra={
                    "stat-time-max": os_stat_profiler.max,
                    "stat-time-mean": os_stat_profiler.mean,
                },
            )
        else:
            # Otherwise note last time progress was made
            status["last-seen"] = time.time()
            self.log.info(
                "%d files with list indices %d-%d (out of %d) found within %.1f seconds",
                files_found,
                status["seen-files"] - files_found + 1,
                status["seen-files"],
                filecount,
                time.time() - status["start-time"],
                extra={
                    "stat-time-max": os_stat_profiler.max,
                    "stat-time-mean": os_stat_profiler.mean,
                },
            )

        # Send results to myself for next round of processing
        rw.checkpoint(
            {"filewatcher-status": status}, delay=message_delay, transaction=txn
        )
        rw.transport.transaction_commit(txn)
