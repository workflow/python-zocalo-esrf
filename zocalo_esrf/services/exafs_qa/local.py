import os
import time

from pprint import pformat
import workflows.recipe
from silx.utils.retry import RetryTimeoutError

from zocalo_esrf.services.exafs_qa.main import EXAFS_QA
from zocalo_esrf.services.exafs_qa.est import run as est
from zocalo_esrf.services.blissfilewatcher import (
    BlissFileWatcher,
    get_last_scan,
    get_hdf5_current_point,
    is_file_selected,
)
from zocalo_esrf.util.files import recursive_mkdir


class EXAFS_QALocal(EXAFS_QA):
    """A local EXAFS QA service combining filewatcher and processor
    to deal with issues of accessing bliss h5 over nfs"""

    _service_name = "EXAFS QA Local"

    _logger_name = "zocalo_esrf.services.exafs_qa_local"

    def initializing(self):
        try:
            beamline = os.environ["BEAMLINE"]
        except KeyError:
            print("BEAMLINE environment variable not defined")
            exit(1)

        queue = f"exafs_qa_{beamline}"
        self.log.info(f"EXAFS QA Local starting, subscribing to {queue}")

        workflows.recipe.wrap_subscribe(
            self._transport,
            queue,
            self.watch_check,
            acknowledgement=True,
            log_extender=self.extend_log,
        )

    def watch_check(self, rw, header, message):
        parameters = rw.recipe_step["parameters"]

        for param in [
            "hdf5",
            "hdf5-path",
            "expected-points",
            "working_directory",
            "results_directory",
        ]:
            if parameters.get(param) is None:
                self.log.error(f"Rejecting message with undefined {param}")
                rw.transport.nack(header)
                return

        self.watch(rw, header, message)

    def watch(self, rw, header, message):
        status = {"seen-points": 0, "start-time": time.time(), "point-count": None}
        if isinstance(message, dict):
            status.update(message.get("filewatcher-status", {}))

        hdf5 = rw.recipe_step["parameters"]["hdf5"]
        hdf5_path = status.get("hdf5-path", rw.recipe_step["parameters"]["hdf5-path"])
        point_count = int(rw.recipe_step["parameters"].get("expected-points"))

        selections = BlissFileWatcher._parse_selections(
            {f"select-{rw.recipe_step['parameters']['select']}": 0}
        )

        self.workingdir = rw.recipe_step["parameters"]["working_directory"]
        self.resultsdir = rw.recipe_step["parameters"]["results_directory"]

        # Conditionally acknowledge receipt of the message
        txn = rw.transport.transaction_begin()
        rw.transport.ack(header, transaction=txn)

        # Look for points
        file_exists = True
        if not os.path.isfile(hdf5):
            self.log.info("File does not yet exist %s", hdf5)
            points_found = 0
            file_exists = False

        # In case of sequence of scans, find latest scan,
        # only on the first message, and persist into status
        if not status.get("file_exists") and file_exists:
            try:
                hdf5_path = get_last_scan(hdf5, hdf5_path, self.log)
                status["hdf5-path"] = hdf5_path
            except KeyError as e:
                self.log.info(
                    "Unable to lookup potential sequence, cant find root: %s", e
                )
                points_found = 0
                file_exists = False
            except Exception:
                self.log.exception("Something went wrong reading the root file")
                points_found = 0
                file_exists = False

            try:
                recursive_mkdir(self.workingdir)
                recursive_mkdir(self.resultsdir)
            except OSError as e:
                self.log.error(
                    "Could not create working and results directories: %s",
                    str(e),
                    exc_info=True,
                )
                self._transport.nack(header)
                self.update_job_status(
                    rw,
                    "processing failed, could not create working directories",
                    "failure",
                )
                return

        status["file_exists"] = file_exists

        if file_exists:
            try:
                points_found = (
                    get_hdf5_current_point(hdf5, hdf5_path) - status["seen-points"]
                )
            except RetryTimeoutError:
                self.log.exception("Could not read hdf5 file within timeout")
                points_found = 0

            if status["seen-points"] == 0 and points_found > 0:
                self.update_job_status(rw, "processing")

            if points_found < 0:
                self.log.info(
                    "Something went wrong watching (%s@)%s, seen-points is greater than current points: %s",
                    hdf5_path,
                    hdf5,
                    point_count,
                )
                rw.transport.transaction_commit(txn)
                self.update_job_status(
                    rw, "processing failed, seen points > current points", "failure"
                )
                return

            for _ in range(points_found):
                for m, _ in selections.items():
                    if is_file_selected(status["seen-points"] + 1, m, point_count):
                        self.process_point(
                            rw, hdf5, hdf5_path, status["seen-points"],
                        )

                status["seen-points"] += 1

        if status["seen-points"] >= point_count:
            if status["seen-points"] > point_count:
                self.log.warning(
                    f"Found more points ({status['seen-points']}) than expected ({point_count}) for {hdf5}"
                )

            self.log.info(
                "All %d points found for (%s@)%s after %.2f seconds.",
                point_count,
                hdf5_path,
                hdf5,
                time.time() - status["start-time"],
            )

            self.update_job_status(rw, "processing successful", "success")

            rw.transport.transaction_commit(txn)
            return

        message_delay = rw.recipe_step["parameters"].get("burst-wait")
        if points_found == 0:
            # If no points were found, check timeout conditions.
            if status["seen-points"] == 0:
                # For first file: relevant timeout is 'timeout-first', with fallback 'timeout', with fallback 1 hour
                timeout = rw.recipe_step["parameters"].get(
                    "timeout-first", rw.recipe_step["parameters"].get("timeout", 3600)
                )
                timed_out = (status["start-time"] + timeout) < time.time()
            else:
                # For subsequent images: relevant timeout is 'timeout', with fallback 1 hour
                timeout = rw.recipe_step["parameters"].get("timeout", 3600)
                timed_out = (status["last-seen"] + timeout) < time.time()

            if timed_out:
                message = (
                    "Filewatcher for (%s@)%s timed out after %.2f seconds (%d points found, nothing seen for %.2f seconds)"
                    % (
                        hdf5_path,
                        hdf5,
                        time.time() - status["start-time"],
                        status["seen-points"],
                        time.time() - status.get("last-seen", status["start-time"]),
                    ),
                )
                self.log.warning(message)
                self.update_job_status(rw, "processing successful", "success")
                self.add_message(rw, "Filewatcher timed out", message, 1)

                rw.transport.transaction_commit(txn)
                return

            # If no timeouts are triggered, set a minimum waiting time.
            if message_delay:
                message_delay = max(1, message_delay)
            else:
                message_delay = 1

            self.log.debug(
                "No further points found for (%s@)%s after a total time of %.2f seconds\n"
                "%s of %s points seen so far",
                hdf5_path,
                hdf5,
                time.time() - status["start-time"],
                status["seen-points"],
                point_count,
            )
        else:
            # Otherwise note last time progress was made
            status["last-seen"] = time.time()
            self.log.info(
                "%d points found for (%s@)%s (total: %d out of %d) within %.2f seconds",
                points_found,
                hdf5_path,
                hdf5,
                status["seen-points"],
                point_count,
                time.time() - status["start-time"],
            )

        # Send results to myself for next round of processing
        rw.checkpoint(
            {"filewatcher-status": status}, delay=message_delay, transaction=txn
        )
        rw.transport.transaction_commit(txn)

    def process_point(self, rw, hdf5, hdf5_path, index):
        if index == 0:
            self.log.info("Ignoring first point")
            return

        start = time.time()

        try:
            inputs = self.get_inputs(hdf5, hdf5_path, index)
        except (RetryTimeoutError, OSError) as e:
            self.log.warning(
                "Could not get inputs, re-queueing message for retry later: %s", e
            )

        for inp in inputs.values():
            if inp is None:
                self.log.warning(
                    "Could not generate all inputs for EXAFS QA from %s:%s",
                    hdf5,
                    hdf5_path,
                )
                return

        output_file = os.path.join(self.resultsdir, f"exafs_qa_{index}.h5")

        try:
            os.chdir(self.workingdir)
            xas_obj = est(
                [
                    "--input-spectra",
                    f"{inputs['spectra']}@{hdf5}",
                    "--input-channel",
                    f"{inputs['energy']}@{hdf5}",
                    "--input-energy-unit",
                    "keV",
                    "--input-I0",
                    f"{inputs['I0']}@{hdf5}",
                    "--input-I1",
                    f"{inputs['I1']}@{hdf5}",
                    "--input-I2",
                    f"{inputs['I2']}@{hdf5}",
                    "--input-mu-ref",
                    f"{inputs['mu_ref']}@{hdf5}",
                    "--set-autobk-params=rbkg:2",
                    f"--set-output-params=output_file:{output_file}",
                    f"--set-energyroi-params=minE:100,maxE:{inputs['energy_end']}",
                ]
            )

        except Exception:
            self.log.exception(
                "EXAFS QA failed on %s:%s with index %s (max_e: %.2f)",
                hdf5,
                hdf5_path,
                index,
                inputs["energy_end"],
            )
        else:
            noise = xas_obj.spectra.data.flat[0].norm_noise_savgol
            runtime = time.time() - start

            ispyb_command_list = []
            ispyb_command_list.append(
                {
                    "ispyb_command": "store_per_image_analysis_results",
                    "no_check": True,
                    "total_intensity": noise,
                    # Images are 1 offset
                    "file-number": index + 1,
                }
            )

            for rtype, rfile in {"Result": output_file}.items():
                ispyb_command_list.append(
                    {
                        "attachment_id": "$ispyb_autoprocprogramattachment_id",
                        "ispyb_command": "add_program_attachment",
                        "file_path": os.path.dirname(rfile),
                        "file_name": os.path.basename(rfile),
                        "file_type": rtype,
                    }
                )

            self.log.debug("Sending %s", pformat(ispyb_command_list))
            rw.send_to("ispyb", {"ispyb_command_list": ispyb_command_list})
            self.log.info("Sent %d commands to ISPyB", len(ispyb_command_list))

            rw.send_to(
                "result",
                {
                    "hdf5": hdf5,
                    "hdf5-path": hdf5_path,
                    "hdf5-index": index,
                    "noise": noise,
                },
            )

            self.log.info(
                "EXAFS QA completed on %s:%s with index %s (max_e: %.2f), noise determined to be %s in %.1f seconds",
                hdf5,
                hdf5_path,
                index,
                inputs["energy_end"],
                noise,
                runtime,
            )

    def update_job_status(self, rw, message, status=None):
        ispyb_command_list = []
        ispyb_command_list.append(
            {
                "ispyb_command": "update_processing_status",
                "message": message,
                "status": status,
            }
        )
        self.log.debug("Sending %s", pformat(ispyb_command_list))
        rw.send_to("ispyb", {"ispyb_command_list": ispyb_command_list})

    def add_message(self, rw, text, summary, level):
        ispyb_command_list = [
            {
                "ispyb_command": "add_program_message",
                "message": text,
                "description": summary,
                "severity": {0: "INFO", 1: "WARNING", 2: "ERROR"}.get(level),
            }
        ]

        self.log.debug("Sending %s", pformat(ispyb_command_list))
        rw.send_to("ispyb", {"ispyb_command_list": ispyb_command_list})
