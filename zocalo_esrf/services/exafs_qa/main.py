import time
import os
from pprint import pformat

import workflows.recipe
from workflows.services.common_service import CommonService

import silx.io.h5py_utils
from silx.utils.retry import RetryTimeoutError

from zocalo_esrf.util.files import recursive_mkdir
from zocalo_esrf.services.exafs_qa.est import run as est


class EXAFS_QA(CommonService):
    """A service to analyse signal quality for exafs."""

    # Human readable service name
    _service_name = "EXAFS QA"

    _logger_name = "zocalo_esrf.services.exafs_qa"

    def initializing(self):
        self.log.info("EXAFS QA starting")

        workflows.recipe.wrap_subscribe(
            self._transport,
            "exafs_qa",
            self.process,
            acknowledgement=True,
            log_extender=self.extend_log,
        )

    @silx.io.h5py_utils.retry(retry_timeout=2)
    def get_inputs(self, file, path, index):
        input_search = {
            "I0": ["I0"],
            "I1": ["I0"],
            "I2": ["I2"],
            "spectra": ["mu_trans"],
            "energy": ["musst_eneenc", "eneenc"],
            "mu_ref": ["mu_ref"],
        }

        inputs = {k: None for k in input_search.keys()}
        with silx.io.h5py_utils.File(file) as h5:
            for d in h5[path]:
                for key, ds in input_search.items():
                    if d in ds:
                        inputs[key] = f"{path}/{d}"

            if inputs["energy"]:
                inputs["energy_end"] = h5[inputs["energy"]][index] * 1000

        return inputs

    def process(self, rw, header, message):
        """Run EXAFS Signal QA"""
        parameters = rw.recipe_step.get("parameters", {})

        if parameters.get("working_directory") is None:
            self.log.error("Rejecting message with undefined working_directory")
            rw.transport.nack(header)
            return

        if parameters.get("results_directory") is None:
            self.log.error("Rejecting message with undefined results_directory")
            rw.transport.nack(header)
            return

        if not isinstance(message, dict):
            self.log.error("Rejecting message with none dict message")
            rw.transport.nack(header)
            return

        if not all(
            [message.get(k) is not None for k in ["hdf5", "hdf5-path", "hdf5-index"]]
        ):
            self.log.error(
                "Rejecting message with undefined hdf5, hdf5-path, or hdf5-index: %s",
                message,
            )
            rw.transport.nack(header)
            return

        workingdir = parameters["working_directory"]
        resultsdir = parameters["results_directory"]

        try:
            recursive_mkdir(workingdir)
            recursive_mkdir(resultsdir)
        except OSError as e:
            self.log.error(
                "Could not create working and results directories: %s",
                str(e),
                exc_info=True,
            )
            self._transport.nack(header)
            return

        start = time.time()

        file = message["hdf5"]
        path = message["hdf5-path"]
        index = message["hdf5-index"]

        if index == 0:
            # No data at this point so cant calculate noise
            self.log.info("Not processing index 0")
            rw.transport.ack(header)
            return

        try:
            inputs = self.get_inputs(file, path, index)
        except (RetryTimeoutError, OSError) as e:
            self.log.warning(
                "Could not get inputs, re-queueing message for retry later: %s", e
            )
            if message.get("retry-count", 0) > 3:
                self.log.error("Message retried 3 times, rejecting")
                rw.transport.nack(header)
            else:
                message["retry-count"] = message.get("retry-count", 0) + 1
                txn = rw.transport.transaction_begin()
                rw.transport.ack(header, transaction=txn)
                rw.checkpoint(message, delay=5, transaction=txn)
                rw.transport.transaction_commit(txn)
            return

        for inp in inputs.values():
            if inp is None:
                self.log.warning(
                    "Could not generate all inputs for EXAFS QA from %s:%s", file, path
                )
                rw.transport.nack(header)
                return

        output_file = os.path.join(resultsdir, f"exafs_qa_{index}.h5")

        args = [
            "--input-spectra",
            f"{inputs['spectra']}@{file}",
            "--input-channel",
            f"{inputs['energy']}@{file}",
            "--input-energy-unit",
            "keV",
            "--input-I0",
            f"{inputs['I0']}@{file}",
            "--input-I1",
            f"{inputs['I1']}@{file}",
            "--input-I2",
            f"{inputs['I2']}@{file}",
            "--input-mu-ref",
            f"{inputs['mu_ref']}@{file}",
            "--set-autobk-params=rbkg:2",
            f"--set-output-params=output_file:{output_file}",
            f"--set-energyroi-params=minE:100,maxE:{inputs['energy_end']}",
        ]

        self.log.info(pformat(args))

        # Conditionally acknowledge receipt of the message
        txn = rw.transport.transaction_begin()
        rw.transport.ack(header, transaction=txn)

        noise = None
        success = True
        try:
            os.chdir(workingdir)
            xas_obj = est(args)

        # For hdf5 timeout we can try again later
        # except est.thirdparty.silx.hdf5file.HDF5TimeoutError:
        #     message_delay = 1
        #     rw.checkpoint(
        #         {"exafs-qa-status": status}, delay=message_delay, transaction=txn
        #     )
        #     rw.transport.transaction_commit(txn)
        #     return

        except Exception:
            success = False
            self.log.exception(
                "EXAFS QA failed on %s:%s with index %s (max_e: %.2f)",
                file,
                path,
                index,
                inputs["energy_end"],
            )
        else:
            noise = xas_obj.spectra.data.flat[0].norm_noise_savgol

        runtime = time.time() - start

        # Update status and store results
        ispyb_command_list = []
        if success:
            ispyb_command_list.append(
                {
                    "ispyb_command": "store_per_image_analysis_results",
                    "no_check": True,
                    "total_intensity": noise,
                    # Images are 1 offset
                    "file-number": index + 1,
                }
            )

            for rtype, rfile in {"Result": output_file}.items():
                ispyb_command_list.append(
                    {
                        "attachment_id": "$ispyb_autoprocprogramattachment_id",
                        "ispyb_command": "add_program_attachment",
                        "file_path": os.path.dirname(rfile),
                        "file_name": os.path.basename(rfile),
                        "file_type": rtype,
                    }
                )

        self.log.debug("Sending %s", pformat(ispyb_command_list))

        rw.set_default_channel("result")
        rw.send_to("ispyb", {"ispyb_command_list": ispyb_command_list}, transaction=txn)
        self.log.info("Sent %d commands to ISPyB", len(ispyb_command_list))

        if success:
            rw.send_to(
                "result",
                {"hdf5": file, "hdf5-path": path, "hdf5-index": index, "noise": noise},
                transaction=txn,
            )

        rw.transport.transaction_commit(txn)

        if success:
            self.log.info(
                "EXAFS QA completed on %s:%s with index %s (max_e: %.2f), noise determined to be %s in %.1f seconds",
                file,
                path,
                index,
                inputs["energy_end"],
                noise,
                runtime,
            )
