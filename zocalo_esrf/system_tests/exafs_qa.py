import os

from pytest import approx
from workflows.recipe import Recipe

from zocalo_test.system.common import CommonSystemTest
from zocalo_test.system.tmp import tmp_folder_date


class EXAFSQAService(CommonSystemTest):
    """Tests for the EXAFS-QA service."""

    def test_exafs_qa(self):
        """Run EXAFS QA on the 10th point of the dataset"""

        hdf5 = self.config["exafs_qa"]["scan_hdf5"]
        hdf5_path = "1.1/measurement"
        hdf5_index = 100

        tmp_folder = os.path.join(tmp_folder_date(self.config["temp_dir"]), self.uuid)
        os.makedirs(tmp_folder)
        os.chmod(tmp_folder, 0o777)

        recipe = {
            1: {
                "service": "EXAFS QA",
                "queue": "exafs_qa",
                "parameters": {
                    "working_directory": tmp_folder,
                    "results_directory": tmp_folder,
                },
                "output": {"result": 2},
            },
            2: {"service": "System Test", "queue": "transient.system_test" + self.uuid},
            "start": [
                (1, {"hdf5": hdf5, "hdf5-path": hdf5_path, "hdf5-index": hdf5_index})
            ],
        }
        recipe = Recipe(recipe)
        recipe.validate()

        self.send_message(
            queue=recipe[1]["queue"],
            message={
                "payload": recipe["start"][0][1],
                "recipe": recipe.recipe,
                "recipe-pointer": "1",
                "environment": {"ID": self.uuid},
            },
            headers={"workflows-recipe": True},
        )

        self.expect_recipe_message(
            environment={"ID": self.uuid},
            recipe=recipe,
            recipe_path=[1],
            recipe_pointer=2,
            payload={
                "hdf5": hdf5,
                "hdf5-path": hdf5_path,
                "hdf5-index": hdf5_index,
                "noise": approx(0.0409, abs=0.0001),
            },
            timeout=30,
        )


if __name__ == "__main__":
    EXAFSQAService().validate()
