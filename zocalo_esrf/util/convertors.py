from pint import UnitRegistry

ureg = UnitRegistry()


def to_energy(wavelength):
    return ((ureg.h * ureg.c) / (wavelength * ureg.angstrom)).to(ureg.eV).magnitude
