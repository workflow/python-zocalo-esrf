import contextlib
from logging.handlers import BufferingHandler
import logging


class LogToFile(BufferingHandler):
    def __init__(self, capacity):
        logging.handlers.BufferingHandler.__init__(self, capacity)

    def flush(self):
        messages = [self.format(record) for record in self.buffer]
        super(LogToFile, self).flush()
        return messages

    def shouldFlush(self, record):
        return False


@contextlib.contextmanager
def log_to_file(filename, level=logging.INFO):
    h = LogToFile(10000)
    h.setLevel(logging.INFO)
    h.setFormatter(
        logging.Formatter("{asctime} {name} {levelname} {message}", style="{")
    )
    logging.getLogger().addHandler(h)
    logging.getLogger().setLevel(level)

    yield

    logging.getLogger().setLevel(logging.WARN)
    logging.getLogger().removeHandler(h)

    with open(filename, "w") as fh:
        fh.write("\n".join(h.flush()))
