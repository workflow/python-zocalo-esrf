import logging

from spectrocrunch.xrf.fit import AdaptPyMcaConfig_modinfo as SCAdaptPyMcaConfig_modinfo

logger = logging.getLogger(__name__)


def AdaptPyMcaConfig_peaks(cfg, peaks):
    cfg["peaks"] = {}
    for peak in peaks:
        el, pk = peak
        if el not in cfg["peaks"]:
            cfg["peaks"][el] = ""

        if cfg["peaks"][el]:
            cfg["peaks"][el] += f", {pk}"
        else:
            cfg["peaks"][el] = pk


def AdaptPyMcaConfig_modinfo(cfg, quant=False):
    SCAdaptPyMcaConfig_modinfo(cfg, quant)

    logger.info(
        "\n Selected Peaks = \n    {}".format(
            "\n    ".join([f"{el} = {peak}" for el, peak in cfg["peaks"].items()])
        )
    )
