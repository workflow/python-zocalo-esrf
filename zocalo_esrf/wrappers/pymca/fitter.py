import re
import os
import logging

from PyMca5.PyMcaIO import ConfigDict
from PyMca5.PyMcaPhysics.xrf import McaAdvancedFitBatch
from PyMca5.PyMcaPhysics.xrf import FastXRFLinearFit
from PyMca5.PyMcaPhysics.xrf.McaAdvancedFitBatch import OutputBuffer
from PyMca5.PyMcaPhysics.xrf.ClassMcaTheory import ClassMcaTheory
from PyMca5.PyMcaIO import HDF5Stack1D

from spectrocrunch.xrf.fit import AdaptPyMcaConfig_energy
from zocalo_esrf.wrappers.pymca.config import (
    AdaptPyMcaConfig_peaks,
    AdaptPyMcaConfig_modinfo,
)

logger = logging.getLogger(__name__)


# https://github.com/vasole/pymca/blob/master/PyMca5/PyMcaGui/physics/xrf/McaAdvancedFit.py#L815
# TODO: Add diagnostics for missing peaks
def diagnostics():
    mcafit = ClassMcaTheory(cfgfile)

    energy = fitresult["result"]["energy"]
    param = fitresult["result"]["fittedpar"]
    i = fitresult["result"]["parameters"].index("Noise")
    noise = param[i] * param[i]
    i = fitresult["result"]["parameters"].index("Fano")
    fano = param[i] * 2.3548 * 2.3548 * 0.00385
    meanfwhm = numpy.sqrt(noise + 0.5 * (energy[0] + energy[-1]) * fano)
    i = fitresult["result"]["parameters"].index("Gain")
    gain = fitresult["result"]["fittedpar"][i]
    meanfwhm = int(meanfwhm / gain) + 1

    missed = mcafit.detectMissingPeaks(y, yfit, meanfwhm)

    over = mcafit.detectMissingPeaks(yfit, y, meanfwhm)


def fit(
    file,
    path,
    detectors,
    monitor,
    cfg,
    outputdir,
    outputfile,
    energy,
    peaks,
    fast=False,
):
    if isinstance(cfg, str):
        cfg = ConfigDict.ConfigDict(filelist=cfg)

    quant = {}

    AdaptPyMcaConfig_peaks(cfg, peaks)
    AdaptPyMcaConfig_energy(cfg, energy, False)
    AdaptPyMcaConfig_modinfo(cfg)

    cfgfile = os.path.join(outputdir, outputfile) + ".cfg"
    cfg.write(cfgfile)

    data_path = re.sub("[\w\.]+\/", "/", path)
    process = "pymca"

    kw = {}
    kw["h5"] = True
    kw["edf"] = False
    kw["outputDir"] = outputdir
    kw["outputRoot"] = outputfile
    kw["fileProcess"] = process
    # kw["saveResiduals"] = True
    # kw["saveFit"] = True
    # kw["saveData"] = True

    results = []
    for detector in detectors:
        kw["fileEntry"] = detector
        outbuffer = OutputBuffer(**kw)

        results.append(f"{detector}/{process}")

        selection = {
            "y": f"{data_path}/{detector}",
            "m": f"{data_path}/{monitor}",
        }

        if fast:
            batch = FastXRFLinearFit.FastXRFLinearFit()
            stack = HDF5Stack1D.HDF5Stack1D([file], selection)  # , scanlist=scanlist)

            kwargs = {
                "y": stack,
                "configuration": cfg,
                "concentrations": bool(quant),
                "refit": 1,
                "outbuffer": outbuffer,
            }
        else:
            kwargs = {
                "filelist": [file],
                "selection": selection,
                # "scanlist": scanlist,
                "concentrations": bool(quant),
                "fitfiles": 0,
                "fitconcfile": 0,
                "outbuffer": outbuffer,
            }
            batch = McaAdvancedFitBatch.McaAdvancedFitBatch(cfgfile, **kwargs)

        with outbuffer.saveContext():
            if fast:
                batch.fitMultipleSpectra(**kwargs)
            else:
                batch.processList()

    return (os.path.join(outputdir, outputfile) + ".h5", results)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    cwd = os.path.dirname(__file__)
    file = os.path.join(cwd, "data", "fluoXAS_Agsat1_0001.h5")
    config = os.path.join(cwd, "data", "3420ev_FX2_leia_Ag.cfg")

    # file = os.path.join(cwd, "data", "EB2_R2_Zr65_incl_1_roi2165_2053.h5")
    # config = os.path.join(cwd, "data", "EB2_R2_Zr65_incl_1_roi2165_2053.cfg")

    print(file, config)

    peaks = [["Al", "K"], ["Si", "K"], ["S", "K"]]
    fit(
        file,
        "1.1/measurement",
        ["fx2_det0"],
        "iodet",
        config,
        os.path.join(cwd, "output"),
        "test",
        2.481,
        peaks,
        fast=True,
    )
