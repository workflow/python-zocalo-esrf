import os
import logging

import chemparse
from PyMca5.PyMcaPhysics import Elements
import silx.io.h5py_utils

import zocalo.configuration
from zocalo.wrapper import BaseWrapper

from zocalo_esrf.util.convertors import to_energy
from zocalo_esrf.util.logging import log_to_file
from zocalo_esrf.util.files import recursive_mkdir
from zocalo_esrf.wrappers.pymca.fitter import fit


logger = logging.getLogger(__name__)


class PyMCAFitter(BaseWrapper):
    def add_messages(self, text, summary, level):

        ispyb_command_list = [
            {
                "ispyb_command": "add_program_message",
                "message": text,
                "description": summary,
                "severity": {0: "INFO", 1: "WARNING", 2: "ERROR"}.get(level),
            }
        ]

        self.recwrap.send_to("ispyb", {"ispyb_command_list": ispyb_command_list})

    # https://github.com/vasole/pymca/blob/master/PyMca5/PyMcaGui/physics/xrf/FitPeakSelect.py#L342
    def get_peaks(self, elements, energy, split_ab=False):
        peaks = []
        for el in elements:
            z = Elements.ElementList.index(el) + 1
            if (z > 47) and (Elements.getomegam5("Cd") > 0.0):
                disabled = []
            elif z > 66:
                disabled = []
            elif z > 17:
                disabled = ["M"]
            elif z > 2:
                disabled = ["L", "L1", "L2", "L3", "M"]
            else:
                disabled = ["Ka", "Kb", "L", "L1", "L2", "L3", "M"]

            peak_list = ["K", "L", "L1", "L2", "L3", "M"]
            if split_ab:
                peak_list.insert(1, "Kb")
                peak_list.insert(1, "Ka")

            for peak in peak_list:
                if peak not in disabled:
                    if peak == "L":
                        if Elements.Element[el]["binding"]["L3"] <= energy:
                            peaks.append([el, peak])
                    elif peak == "M":
                        if Elements.Element[el]["binding"]["M5"] <= energy:
                            peaks.append([el, peak])
                    elif peak == "Ka":
                        if Elements.Element[el]["binding"]["K"] <= energy:
                            peaks.append([el, peak])
                    elif peak == "Kb":
                        if Elements.Element[el]["binding"]["K"] <= energy:
                            peaks.append([el, peak])
                    elif Elements.Element[el]["binding"][peak] <= energy:
                        peaks.append([el, peak])

        return peaks

    def get_datasets(self, file, path, monitors=["iodet"]):
        detectors = []
        monitor = None

        with silx.io.h5py_utils.File(file) as h5:
            for gname in h5[path]:
                group = h5[f"{path}/{gname}"]
                if len(group.shape) == 2:
                    detectors.append(gname)

                if gname in monitors:
                    monitor = gname

        return detectors, monitor

    def parse_results(self, file, paths, peaks):
        maps = []
        try:
            with silx.io.h5py_utils.File(file) as h5:
                for path in paths:
                    root = h5[f"{path}/results/parameters"]

                    for peak in peaks:
                        dset = f"{peak[0]}_{peak[1]}"
                        if dset in root:
                            # TODO: deal with multiple spectra (fluoxas)
                            maps.append({"peak": peak, "data": root[dset][0].tolist()})
        except:
            logger.exception("Could not parse results")
            return

        return maps

    def run(self):
        assert hasattr(self, "recwrap"), "No recipewrapper object found"

        parameters = self.recwrap.recipe_step["job_parameters"]

        if not parameters.get("composition"):
            logger.warning("No composition defined, not processing")
            self.add_messages(
                "No composition defined", "Component has no composition defined", 1
            )
            return

        if not parameters.get("wavelength"):
            logger.warning("No wavelength defined, not processing")
            pass

        resultsdir = parameters["results_directory"]

        try:
            recursive_mkdir(resultsdir)
        except OSError as e:
            self.log.error(
                "Could not create results directories: %s", str(e), exc_info=True,
            )
            return

        configs = []
        # config in visit dir
        configs.append(
            os.path.join(parameters["visit_directory"], "pymca", "pymca.cfg")
        )

        zc = zocalo.configuration.from_file()
        zc.activate()

        # global default
        if zc.storage.get("pymca_config_root"):
            configs.append(os.path.join(zc.storage["pymca_config_root"], "pymca.cfg"))
        cfg = None

        for conf in configs:
            if os.path.exists(conf):
                logger.info("Using config: %s", conf)
                cfg = conf
                break

        if cfg is None:
            logger.error("Could not find a pymca config file, tried: %s", configs)
            self.add_messages(
                "No config found",
                f"Could not find a pymca config file, tried: {configs}",
                1,
            )
            return

        energy = to_energy(parameters["wavelength"]) / 1000
        elements = chemparse.parse_formula(parameters["composition"])
        peaks = self.get_peaks(elements.keys(), energy)

        detectors, monitor = self.get_datasets(
            parameters["hdf5"],
            parameters["hdf5-path"],
            parameters.get("monitors", ["iodet"]),
        )

        if parameters.get("single_detector", True):
            detectors = [detectors[0]]

        logger.info("detectors %s, monitor %s", detectors, monitor)

        resp = None
        log_file = os.path.join(resultsdir, "pymca.log")
        with log_to_file(log_file, level=logging.DEBUG):
            logger.info("Using config: %s", cfg)
            try:
                resp = fit(
                    parameters["hdf5"],
                    parameters["hdf5-path"],
                    detectors,
                    monitor,
                    cfg,
                    resultsdir,
                    os.path.basename(parameters["hdf5"]).replace(".h5", "_pymca"),
                    energy,
                    peaks,
                    fast=parameters.get("fast", False),
                )
                success = True
            except:
                logger.exception("PyMCA failed")
                success = False

        if resp:
            file, results = resp
        else:
            success = False
            logger.error("PyMCA failed")

        if success:
            points = parameters["steps_x"] * parameters["steps_y"]
            ispyb_command_list = []
            maps = self.parse_results(file, results, peaks)
            if maps is None:
                success = False
            else:
                for map_ in maps:
                    # TODO: reuse ROIs
                    ispyb_command_list.append(
                        {
                            "ispyb_command": "upsert_fluo_mapping_roi",
                            "element": map_["peak"][0],
                            "edge": f"{map_['peak'][1]}-pymca",
                            "start_energy": 0,
                            "end_energy": 0,
                            "store_result": "ispyb_xrf_map_roi_id",
                        }
                    )
                    ispyb_command_list.append(
                        {
                            "ispyb_command": "upsert_fluo_mapping",
                            "roi_id": "$ispyb_xrf_map_roi_id",
                            "data": map_["data"],
                            "data_format": "json+gzip",
                            "points": points,
                        }
                    )
                logger.info("Sending %s", str(ispyb_command_list))
                self.recwrap.send_to(
                    "ispyb", {"ispyb_command_list": ispyb_command_list}
                )

            self.record_result_individual_file(
                {
                    "file_path": os.path.dirname(file),
                    "file_name": os.path.basename(file),
                    "file_type": "Result",
                }
            )

            self.record_result_individual_file(
                {
                    "file_path": os.path.dirname(log_file),
                    "file_name": os.path.basename(log_file),
                    "file_type": "Log",
                }
            )

        return success
